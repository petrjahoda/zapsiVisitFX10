package sample;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class User
        implements Serializable {
    private String userName;
    private String userPassword;
    private String userChipCode;
    private String userCreateTime;
    private boolean userIsAdmin;
    private static final long serialVersionUID = 1234;

    public StringProperty userNameProperty() {
        return new SimpleStringProperty(this.userName);
    }

    public StringProperty userPasswordProperty() {
        return new SimpleStringProperty(this.userPassword);
    }

    public StringProperty userChipCodeProperty() {
        return new SimpleStringProperty(this.userChipCode);
    }

    public StringProperty userCreateTimeProperty() {
        return new SimpleStringProperty(this.userCreateTime);
    }

    public BooleanProperty userIsAdminProperty() {
        return new SimpleBooleanProperty(this.userIsAdmin);
    }

    User(String userName, String userPassword, String userChipCode) {
        this.userCreateTime = this.currentTimeAsText();
        this.userName = userName;
        this.userPassword = userPassword;
        this.userChipCode = userChipCode;
        this.userIsAdmin = false;
    }

    User(String userName, String userPassword) {
        this.userCreateTime = this.currentTimeAsText();
        this.userName = userName;
        this.userPassword = userPassword;
        this.userIsAdmin = false;
    }

    void setUserName(String userName) {
        this.userName = userName;
    }

    void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    void setUserChipCode(String userChipCode) {
        this.userChipCode = userChipCode;
    }

    void setUserIsAdmin(boolean isAdmin) {
        this.userIsAdmin = isAdmin;
    }

    String getUserName() {
        return this.userName;
    }

    String getUserPassword() {
        return this.userPassword;
    }

    boolean isUserIsAdmin() {
        return this.userIsAdmin;
    }

    public String toString() {
        return this.userName + ", " + this.userPassword + ", " + this.userChipCode;
    }

    private String currentTimeAsText() {
        double actualTime = System.currentTimeMillis();
        return new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(actualTime);
    }
}

