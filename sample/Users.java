package sample;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;

class Users
        implements Serializable {
    static ArrayList<User> users;

    Users() {
    }

    static void addUser(User actualUser) {
        users.add(actualUser);
    }

    static void removeUser(User actualUser) {
        try {
            users.remove(actualUser);
        }
        catch (Exception userNotExists) {
            System.out.println("ERROR: user does not exist.");
        }
    }

    static void initializeUsers() {
        users = new ArrayList(0);
        users = Users.loadUsersFromFile();
        if (users.size() == 0) {
            Users.createDefaultUser();
        }
    }

    private static void createDefaultUser() {
        User defaultUser = new User("zapsi_uzivatel", "zapsi");
        defaultUser.setUserIsAdmin(true);
        users.add(defaultUser);
    }

    static void removeBadlyInsertedUsers() {
        for (int actualUser = 0; actualUser < users.size(); ++actualUser) {
            User user = users.get(actualUser);
            if (!user.getUserName().isEmpty()) continue;
            users.remove(actualUser);
            --actualUser;
        }
    }

    static void updateUserData(String selectedUserName, int actualUserColumn, String updatedValue) {
        boolean userIsAdmin = false;
        if (updatedValue.equals("ADMIN_IS_TRUE")) {
            userIsAdmin = true;
        }
        User temporaryUser = Users.returnUserForUsername(selectedUserName);
        switch (actualUserColumn) {
            case 1: {
                temporaryUser.setUserName(updatedValue);
                break;
            }
            case 2: {
                temporaryUser.setUserPassword(updatedValue);
                break;
            }
            case 3: {
                temporaryUser.setUserChipCode(updatedValue);
                break;
            }
            case 4: {
                temporaryUser.setUserIsAdmin(userIsAdmin);
                break;
            }
        }
    }

    private static ArrayList<User> loadUsersFromFile() {
        try {
            FileInputStream dataFileReader = new FileInputStream(Main.directory + "/users.dat");
            ObjectInputStream dataStream = new ObjectInputStream(dataFileReader);
            users = (ArrayList)dataStream.readObject();
            dataStream.close();
        }
        catch (IOException | ClassNotFoundException ex) {
            System.out.println("ERROR: Users were not downloaded from file.");
        }
        return users;
    }

    private static User returnUserForUsername(String username) {
        User temporaryUser = null;
        for (User user : users) {
            if (!user.getUserName().equals(username)) continue;
            temporaryUser = user;
        }
        try {
            assert (temporaryUser != null);
        }
        catch (Exception exception) {
            // empty catch block
        }
        return temporaryUser;
    }

    static ArrayList<User> returnUsers() {
        return users;
    }

    static User returnUserWithName(String username) {
        User actualUser = null;
        for (User user : users) {
            if (!user.getUserName().equals(username)) continue;
            actualUser = user;
        }
        return actualUser;
    }

    static boolean userExist(String username) {
        boolean userExist = false;
        for (User user : users) {
            if (!user.getUserName().equals(username)) continue;
            userExist = true;
        }
        return userExist;
    }

    boolean passwordMatchesUser(String username, String password) {
        boolean passwordMatchesUser = false;
        for (User actualUser : users) {
            boolean usernameIsRight = actualUser.getUserName().equals(username);
            boolean passwordIsRight = actualUser.getUserPassword().equals(password);
            if (!usernameIsRight || !passwordIsRight) continue;
            passwordMatchesUser = true;
        }
        return passwordMatchesUser;
    }
}

