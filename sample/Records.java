/*
 * Decompiled with CFR 0_122.
 */
package sample;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

class Records
        implements Serializable {
    static double counter = 0.0;
    static ArrayList<Record> records;

    Records() {
    }

    static void initializeRecords() {
        records = new ArrayList<>(0);
        records = Records.loadRecordsFromFile();
        if (records.size() == 0) {
            counter = 0;
//            Records.createDemoRecords();
        } else {
            counter = records.size() + 1;
        }
    }

    static void addRecord(Record record) {
        records.add(0, record);
        counter += 1.0;
    }

    static void removeRecord(Record record) {
        records.remove(record);
    }

    static void updateRecordWithNewData(double recordID, int positionInTable, String string) {
        boolean recordIsClosed = false;
        if (string.equals("TRUE")) {
            recordIsClosed = true;
        }
        Record temp = Records.returnRecordForID(recordID);
        switch (positionInTable) {
            case 2: {
                temp.setRecordVisitorName(string);
                break;
            }
            case 3: {
                temp.setRecordCompanyName(string);
                break;
            }
            case 4: {
                temp.setRecordVisitorID(string);
                break;
            }
            case 5: {
                temp.setRecordCarID(string);
                break;
            }
            case 6: {
                temp.setRecordVisitReason(string);
                break;
            }
            case 7: {
                temp.setRecordVisitedName(string);
                break;
            }
            case 8: {
                temp.setRecordInternalID(string);
                break;
            }
            case 9: {
                temp.setRecordExitTime(string);
                break;
            }
            case 10: {
                temp.setRecordClosed(recordIsClosed);
                break;
            }
        }
    }

    private static void createDemoRecords() {
        long numberOdMillisecondsBackToGenerate = 63113904000L;
        long actualTime = System.currentTimeMillis();
        User petrJahoda = new User("Petr Jahoda", "54321");
        if (!Users.userExist(petrJahoda.getUserName())) {
            Users.addUser(petrJahoda);
        }
        for (int actualRecord = 0; actualRecord < 1000; ++actualRecord) {
            long recordEntryTime = Records.generateRandomEntryTimeBetween(actualTime, numberOdMillisecondsBackToGenerate);
            Records.insertRecord(recordEntryTime, petrJahoda, actualRecord);
            counter = records.size();
        }
    }

    private static void insertRecord(long recordEntryTime, User petrJahoda, int i) {
        if (i % 2 == 0) {
            Record record = new Record(petrJahoda, "Karel Vom\u00e1\u010dka", "Zapsi", "" + i + "", "1B2 2332", "servis", "" + i + "", "Radek Semansk\u00fd", i, recordEntryTime);
            records.add(record);
        } else if (i % 3 == 0) {
            Record record = new Record(petrJahoda, "Zden\u011bk Ku\u010dera", "Apple", "" + i + "", "1B1 1111", "byznys", "" + i + "", "Steve Jobs", i, recordEntryTime);
            records.add(record);
        } else if (i % 5 == 0) {
            Record record = new Record(petrJahoda, "Luk\u00e1\u0161 Marmol", "Microsoft", "" + i + "", "1B1 2222", "\u0161kolen\u00ed", "" + i + "", "Bill Gates", i, recordEntryTime);
            records.add(record);
        } else if (i % 7 == 0) {
            Record record = new Record(petrJahoda, "Robin Kone\u010dn\u00fd", "Red Hat", "" + i + "", "1B1 3333", "konzultace", "" + i + "", "Linus Torvalds", i, recordEntryTime);
            records.add(record);
        } else {
            Record record = new Record(petrJahoda, "Petr Jahoda", "ANO bude l\u00edp", "" + i + "", "1B1 1234", "\u0161kolen\u00ed", "" + i + "", "Andrej Babi\u0161", i, recordEntryTime);
            records.add(record);
        }
    }

    private static ArrayList<Record> loadRecordsFromFile() {
        try {
            FileInputStream dataFileReader = new FileInputStream(Main.directory + "/records.dat");
            ObjectInputStream dataStream = new ObjectInputStream(dataFileReader);
            records = (ArrayList<Record>) dataStream.readObject();
            dataStream.close();
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("\tERROR: Records were not downloaded from file.");
        }
        return records;
    }

    static ArrayList<Record> returnRecords() {
        return records;
    }

    private static Record returnRecordForID(double recordID) {
        Record temporaryRecord = null;
        for (Record record : records) {
            if (record.getRecordID() != recordID) continue;
            temporaryRecord = record;
        }
        return temporaryRecord;
    }

    private static long generateRandomEntryTimeBetween(long actualTime, long intervalBack) {
        return ThreadLocalRandom.current().nextLong(actualTime - intervalBack, actualTime);
    }


    static ArrayList returnRecordsMatching(String searchValue) {
        return records.stream().filter(record -> record.toString().contains(searchValue)).collect(Collectors.toCollection(ArrayList::new));
    }

    static ArrayList returnRecordsBetween(double initialMillis, double endingMillis) {
        return records.stream().filter(record -> record.getRecordEntryTimeMillis() > initialMillis && record.getRecordEntryTimeMillis() < endingMillis).collect(Collectors.toCollection(ArrayList::new));
    }
}

