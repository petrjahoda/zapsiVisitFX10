/*
 * Decompiled with CFR 0_122.
 *
 * Could not load the following classes:
 *  javafx.beans.property.BooleanProperty
 *  javafx.beans.property.SimpleBooleanProperty
 *  javafx.beans.property.SimpleStringProperty
 *  javafx.beans.property.StringProperty
 */
package sample;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.Serializable;
import java.text.SimpleDateFormat;

public class Record
        implements Serializable {
    private String recordUserName;
    private String recordEntryTime;
    private String recordExitTime;
    private String recordVisitorName;
    private String recordCompanyName;
    private String recordVisitorID;
    private String recordCarID;
    private String recordVisitReason;
    private String recordVisitedName;
    private String recordInternalID;
    private double recordID;
    private double recordEntryTimeMillis;
    private boolean recordClosed;
    private static final long serialVersionUID = 1234;

    public StringProperty recordUserNameProperty() {
        return new SimpleStringProperty(this.recordUserName);
    }

    public StringProperty recordExitTimeProperty() {
        return new SimpleStringProperty(this.recordExitTime);
    }

    public StringProperty recordEntryTimeProperty() {
        return new SimpleStringProperty(this.recordEntryTime);
    }

    public StringProperty recordVisitorNameProperty() {
        return new SimpleStringProperty(this.recordVisitorName);
    }

    public StringProperty recordCompanyNameProperty() {
        return new SimpleStringProperty(this.recordCompanyName);
    }

    public StringProperty recordVisitorIDProperty() {
        return new SimpleStringProperty(this.recordVisitorID);
    }

    public StringProperty recordCarIDProperty() {
        return new SimpleStringProperty(this.recordCarID);
    }

    public StringProperty recordVisitReasonProperty() {
        return new SimpleStringProperty(this.recordVisitReason);
    }

    public StringProperty recordVisitedNameProperty() {
        return new SimpleStringProperty(this.recordVisitedName);
    }

    public StringProperty recordInternalIDProperty() {
        return new SimpleStringProperty(this.recordInternalID);
    }

    public BooleanProperty recordClosedProperty() {
        return new SimpleBooleanProperty(this.recordClosed);
    }

    public StringProperty recordEntryTimeMillisProperty() {
        return new SimpleStringProperty(String.valueOf(this.recordEntryTimeMillis));
    }

    Record(User recordUserName, String recordVisitorName, String recordCompanyName, String recordVisitorID, String recordCarID, String recordVisitReason, String recordInternalID, String recordVisitedName, double recordID, long time) {
        this.recordUserName = recordUserName.getUserName();
        this.recordEntryTime = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(time);
        this.recordEntryTimeMillis = time;
        this.recordVisitReason = recordVisitReason;
        this.recordVisitedName = recordVisitedName;
        this.recordCompanyName = recordCompanyName;
        this.recordVisitorName = recordVisitorName;
        this.recordVisitorID = recordVisitorID;
        this.recordCarID = recordCarID;
        this.recordInternalID = recordInternalID;
        this.recordExitTime = "";
        this.recordID = recordID;
        this.recordClosed = false;
    }

    Record(User recordUserName, String recordVisitReason, String recordVisitedName, double recordID) {
        this.recordUserName = recordUserName.getUserName();
        this.recordEntryTime = this.currentTimeAsText();
        this.recordEntryTimeMillis = System.currentTimeMillis();
        this.recordVisitReason = recordVisitReason;
        this.recordVisitedName = recordVisitedName;
        this.recordCompanyName = "";
        this.recordVisitorName = "";
        this.recordVisitorID = "";
        this.recordCarID = "";
        this.recordInternalID = "";
        this.recordExitTime = "";
        this.recordID = recordID;
        this.recordClosed = false;
    }

    String getRecordUserName() {
        return this.recordUserName;
    }

    String getRecordEntryTime() {
        return this.recordEntryTime;
    }

    String getRecordExitTime() {
        return this.recordExitTime;
    }

    String getRecordVisitorName() {
        return this.recordVisitorName;
    }

    String getRecordCompanyName() {
        return this.recordCompanyName;
    }

    String getRecordVisitorID() {
        return this.recordVisitorID;
    }

    String getRecordCarID() {
        return this.recordCarID;
    }

    String getRecordVisitedName() {
        return this.recordVisitedName;
    }

    String getRecordVisitReason() {
        return this.recordVisitReason;
    }

    double getRecordEntryTimeMillis() {
        return this.recordEntryTimeMillis;
    }

    double getRecordID() {
        return this.recordID;
    }

    void setRecordExitTime(String recordExitTime) {
        this.recordExitTime = recordExitTime;
    }

    void setRecordVisitorName(String recordVisitorName) {
        this.recordVisitorName = recordVisitorName;
    }

    void setRecordCompanyName(String recordCompanyName) {
        this.recordCompanyName = recordCompanyName;
    }

    void setRecordVisitorID(String recordVisitorID) {
        this.recordVisitorID = recordVisitorID;
    }

    void setRecordCarID(String recordCarID) {
        this.recordCarID = recordCarID;
    }

    void setRecordVisitReason(String recordVisitReason) {
        this.recordVisitReason = recordVisitReason;
    }

    void setRecordVisitedName(String recordVisitedName) {
        this.recordVisitedName = recordVisitedName;
    }

    void setRecordInternalID(String recordInternalID) {
        this.recordInternalID = recordInternalID;
    }

    void setRecordClosed(boolean recordClosed) {
        this.recordClosed = recordClosed;
    }

    public String toString() {
        String returnInformation = this.recordUserName + ":" + this.recordEntryTime + ":" + this.recordExitTime + ":" + this.recordVisitorName + ":" + this.recordCompanyName + ":" + this.recordVisitorID + ":" + this.recordCarID + ":" + this.recordVisitReason + ":" + this.recordVisitedName + ":" + this.recordInternalID + ":" + this.recordClosed;
        return returnInformation.toLowerCase();
    }

    private String currentTimeAsText() {
        double actualTime = System.currentTimeMillis();
        return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(actualTime);
    }

    double recordClosed() {
        double visitorIsOut = 0.0;
        if (this.recordClosed) {
            visitorIsOut = 1.0;
        }
        return visitorIsOut;
    }
}

