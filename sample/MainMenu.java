package sample;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.FillPatternType;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class MainMenu implements Initializable {
    public BorderPane window;
    public Button newRecordButton;
    public TextField searchTextField;
    public Button clearSearchButton;
    public Button deleteButton;
    public Button exportButton;
    public Text simpleStatistics;
    public Text helpField;
    public Tab recordsTab;
    public TableView recordsTable;
    public TableColumn recordEntryMillisColumn;
    public TableColumn recordUserNameColumn;
    public TableColumn recordEntryTimeColumn;
    public TableColumn recordVisitorNameColumn;
    public TableColumn recordCompanyNameColumn;
    public TableColumn recordVisitorIDColumn;
    public TableColumn recordCarIDColumn;
    public TableColumn recordVisitReasonColumn;
    public TableColumn recordVisitedNameColumn;
    public TableColumn recordIDColumn;
    public TableColumn recordExitTimeColumn;
    public TableColumn recordClosedColumn;
    public Tab settingsTab;
    public TextField preferencesTextField;
    public Button preferencesButton;
    public TableView usersTable;
    public TableColumn userCreatedColumn;
    public TableColumn userNameColumn;
    public TableColumn userPasswordColumn;
    public TableColumn userChipCodeColumn;
    public TableColumn userIsAdminColumn;
    public Tab statisticsTab;
    public SplitPane mainSplitPane;
    public SplitPane topSplitPane;
    public AnchorPane leftAnchorPane;
    public PieChart topCompaniesPie;
    public PieChart topReasonsPie;
    public BarChart last30DaysChart;
    public NumberAxis thirtyDaysYAxis;
    public Slider chartSlider;
    public Text sliderInfoText;
    private ObservableList<Record> listOfRecords;
    private ObservableList<User> listOfUsers;
    private double selectedRecordID;
    private String selectedUserName;
    private int newUserColumn;
    private boolean isNewUser;
    private int newRecordColumn;
    private boolean isNewRecord;
    private String editedRecordValue;
    private String editedUserValue;
    private int actualUserRow;
    private int actualRecordColumn;
    private boolean usernameExists;
    private int actualUserColumn;
    private double daysToDisplay = 30;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        listOfUsers = FXCollections.observableArrayList();
        listOfRecords = FXCollections.observableArrayList();
        setRecordsTable();
        setUsersTable();
        initiateGUI();
    }

    private void initiateGUI() {
        clearSearchButton.setDisable(true);
        recordExitTimeColumn.setSortable(false);
        preferencesTextField.setText(Main.directory);
        topSplitPane.setStyle("-fx-border-color: F4F4F4");
        mainSplitPane.setStyle("-fx-border-color: F4F4F4");
        preferencesButton.setStyle("-fx-background-radius: 0 3 3 0");
        helpField.setText("CTRL+N (new), CTRL+F (search), DEL (delete), CTRL+E (export)");
        initiateSliderValues();
        initiateSettingsTab();
        initiateTableColoring();
        updateInfoStatistics();
    }

    private void updateInfoStatistics() {
        double actualTime = System.currentTimeMillis();
        String date = new SimpleDateFormat("HH:mm:ss").format(actualTime);
        simpleStatistics.setText("Logged from " + date + ": " + Controller.userName + "          Records displayed: " + recordsTable.getItems().size() + "/" + Records.records.size());
    }

    private void initiateTableColoring() {
        recordsTable.setRowFactory(tableView -> {
                    TableRow row = new TableRow();
                    row.itemProperty().addListener((obs, previousValue, currentValue) -> colorRow(row, (Record) currentValue)
                    );
                    return row;
                }
        );
    }

    private void colorRow(TableRow row, Record currentValue) {
        PseudoClass recordIsOpen = null;
        PseudoClass recordIsClosed = null;
        if (currentValue != null) {
            row.pseudoClassStateChanged(recordIsOpen, currentValue.recordClosed() > 0.0);
            row.pseudoClassStateChanged(recordIsClosed, currentValue.recordClosed() <= 0.0);
        } else {
            row.pseudoClassStateChanged(recordIsOpen, false);
            row.pseudoClassStateChanged(recordIsClosed, false);
        }
    }

    private void initiateSettingsTab() {
        User user = Users.returnUserWithName(Controller.userName);
        if (!user.isUserIsAdmin()) {
            settingsTab.setDisable(true);
        }
    }

    private void initiateSliderValues() {
        chartSlider.setValue(1.0);
        sliderInfoText.setText("last 30 days");
    }

    private void setUsersTable() {
        bindUsersTableToUserClass();
        alignUsersTableColumns();
        setUsersTableEditable();
        initiateUsersTableData();
        setUsersTableResizable();
    }

    private void setUsersTableResizable() {
        usersTable.widthProperty().addListener((obs, oldVal, newVal) -> updateUsersTableWidth()
        );
        usersTable.getVisibleLeafColumns().addListener((ListChangeListener<? super TableColumn<User, ?>>) c -> {
                    updateUsersTableWidth();
                }
        );
    }

    private void updateUsersTableWidth() {
        int userIsAdminColumnFixedWidth = 100;
        int additionalWidth = 10;
        int numberOfVisibleColumns = usersTable.getVisibleLeafColumns().size() - 1;
        double calculatedWidth = usersTable.getWidth() - (double) (userIsAdminColumnFixedWidth + additionalWidth);
        double columnWidth = calculatedWidth / (double) numberOfVisibleColumns;
        userPasswordColumn.setMinWidth(columnWidth);
        userCreatedColumn.setMinWidth(columnWidth);
        userNameColumn.setMinWidth(columnWidth);
        userChipCodeColumn.setMinWidth(columnWidth);
        userIsAdminColumn.setMinWidth((double) userIsAdminColumnFixedWidth);
    }

    private void initiateUsersTableData() {
        listOfUsers.setAll(Users.returnUsers());
        usersTable.setItems(listOfUsers);
        usersTable.setEditable(true);
        sortUsersTableByDateCreated();
    }

    private void sortUsersTableByDateCreated() {
        userCreatedColumn.setSortType(TableColumn.SortType.DESCENDING);
        usersTable.getSortOrder().add(userCreatedColumn);
        userCreatedColumn.setSortable(true);
        usersTable.sort();
    }

    private void setUsersTableEditable() {
        userCreatedColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        userNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        userPasswordColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        userChipCodeColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        userIsAdminColumn.setCellFactory(p -> setUserCheckBoxCell());
    }

    private TableCell<User, Boolean> setUserCheckBoxCell() {
        final CheckBox checkBox = new CheckBox();
        TableCell<User, Boolean> tableCell = new TableCell<>() {

            protected void updateItem(Boolean item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setGraphic(null);
                } else {
                    setGraphic(checkBox);
                    checkBox.setSelected(item.booleanValue());
                }
            }
        };
        checkBox.addEventFilter(MouseEvent.MOUSE_PRESSED, arg_0 -> setUserCheckboxCell(checkBox, tableCell, arg_0));
        return tableCell;
    }

    private void setUserCheckboxCell(CheckBox checkBox, TableCell<User, Boolean> tableCell, MouseEvent event) {
        changeUserStatus(checkBox, tableCell.getTableRow().getItem(), event);
    }

    private void changeUserStatus(CheckBox checkBox, User user, MouseEvent event) {
        event.consume();
        selectedUserName = user.getUserName();
        if (!checkBox.isSelected()) {
            setUserAsAdmin(checkBox);
        } else {
            setUserAsNotAdmin(checkBox, user);
        }
    }

    private void setUserAsNotAdmin(CheckBox checkBox, User user) {
        if (userWantsToChangeDefaultUser(user)) {
            Alert changeAlert = new Alert(Alert.AlertType.WARNING);
            changeAlert.setTitle("ALERT!!!");
            changeAlert.setHeaderText("Not possible to change this user.");
            changeAlert.showAndWait();
        } else {
            checkBox.setSelected(false);
            changeUserAdminState(selectedUserName, false);
        }
    }

    private boolean userWantsToChangeDefaultUser(User user) {
        return user.getUserName().equals("zapsi_uzivatel");
    }

    private void setUserAsAdmin(CheckBox checkBox) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Change settings");
        alert.setHeaderText("Do you really want to set the user as admin?");
        Optional<ButtonType> result = alert.showAndWait();
        if (userConfirmsChange(result)) {
            changeUserAdminState(selectedUserName, true);
            checkBox.setSelected(true);
        } else {
            checkBox.setSelected(false);
        }
    }

    private void changeUserAdminState(String selectedUserName, boolean changeToAdmin) {
        int columnForUpdating = 4;
        if (changeToAdmin) {
            Users.updateUserData(selectedUserName, columnForUpdating, "ADMIN_IS_TRUE");
        } else {
            Users.updateUserData(selectedUserName, columnForUpdating, "ADMIN_IS_FALSE");
        }
    }

    private void alignUsersTableColumns() {
        userCreatedColumn.setStyle("-fx-alignment: center");
        userPasswordColumn.setStyle("-fx-alignment: center");
        userIsAdminColumn.setStyle("-fx-alignment: center");
        userChipCodeColumn.setStyle("-fx-alignment: center");
        userNameColumn.setStyle("-fx-alignment: center");
    }

    private void bindUsersTableToUserClass() {
        userCreatedColumn.setCellValueFactory(new PropertyValueFactory("userCreateTime"));
        userNameColumn.setCellValueFactory(new PropertyValueFactory("userName"));
        userPasswordColumn.setCellValueFactory(new PropertyValueFactory("userPassword"));
        userChipCodeColumn.setCellValueFactory(new PropertyValueFactory("userChipCode"));
        userIsAdminColumn.setCellValueFactory(new PropertyValueFactory("userIsAdmin"));
    }

    private void setRecordsTable() {
        bindRecordsTableToRecordClass();
        alignRecordsTableColumns();
        setRecordsTableEditable();
        initiateRecordsTableData();
        setRecordsTableResizable();
        recordsTable.getSelectionModel().setCellSelectionEnabled(true);
    }

    private void setRecordsTableResizable() {
        recordsTable.widthProperty().addListener((obs, oldVal, newVal) -> {
                    updateRecordsTableWidth();
                }
        );
        recordsTable.getVisibleLeafColumns().addListener((ListChangeListener<? super TableColumn<Record, ?>>) c -> {
                    updateRecordsTableWidth();
                }
        );
    }

    private void updateRecordsTableWidth() {
        int recordClosedColumnFixedWidth = 50;
        int dateColumnsFixedWidth = 120;
        int additionalWidth = 10;
        int numberOfVisibleColumns = recordsTable.getVisibleLeafColumns().size() - 3;
        double calculatedWidth = recordsTable.getWidth() - (double) (recordClosedColumnFixedWidth + additionalWidth + dateColumnsFixedWidth + dateColumnsFixedWidth);
        double columnWidth = calculatedWidth / (double) numberOfVisibleColumns;
        recordEntryTimeColumn.setMinWidth((double) dateColumnsFixedWidth);
        recordExitTimeColumn.setMinWidth((double) dateColumnsFixedWidth);
        recordUserNameColumn.setMinWidth(columnWidth);
        recordVisitedNameColumn.setMinWidth(columnWidth);
        recordIDColumn.setMinWidth(columnWidth);
        recordCompanyNameColumn.setMinWidth(columnWidth);
        recordVisitReasonColumn.setMinWidth(columnWidth);
        recordCarIDColumn.setMinWidth(columnWidth);
        recordVisitorIDColumn.setMinWidth(columnWidth);
        recordVisitorNameColumn.setMinWidth(columnWidth);
        recordClosedColumn.setMinWidth((double) recordClosedColumnFixedWidth);
    }

    private void initiateRecordsTableData() {
        listOfRecords.setAll(Records.returnRecords());
        recordsTable.setItems(listOfRecords);
        recordsTable.setEditable(true);
        sortRecordsTableByDateCreated();
    }

    private void setRecordsTableEditable() {
        recordVisitorNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        recordCompanyNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        recordVisitorIDColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        recordCarIDColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        recordVisitReasonColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        recordVisitedNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        recordIDColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        recordClosedColumn.setCellFactory(p -> setRecordCheckBoxCell());
    }

    private Object setRecordCheckBoxCell() {
        final CheckBox checkBox = new CheckBox();
        TableCell<Record, Boolean> tableCell = new TableCell<>() {

            protected void updateItem(Boolean item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setGraphic(null);
                } else {
                    setGraphic(checkBox);
                    checkBox.setSelected(item.booleanValue());
                }
            }
        };
        checkBox.addEventFilter(MouseEvent.MOUSE_PRESSED, arg_0 -> setRecordCheckBoxCell(checkBox, tableCell, arg_0));
        return tableCell;
    }

    private void setRecordCheckBoxCell(CheckBox checkBox, TableCell<Record, Boolean> tableCell, MouseEvent event) {
        changeRecordStatus(checkBox, tableCell.getTableRow().getItem(), event);
    }

    private void changeRecordStatus(CheckBox checkBox, Record item, MouseEvent event) {
        event.consume();
        selectedRecordID = item.getRecordID();
        if (!checkBox.isSelected()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Exit");
            alert.setHeaderText("Do you really want to save exit?");
            Optional<ButtonType> result = alert.showAndWait();
            if (userConfirmsChange(result)) {
                changeRecordClosedState(selectedRecordID, true);
                checkBox.setSelected(true);
            } else {
                checkBox.setSelected(false);
            }
        } else {
            checkBox.setSelected(false);
            changeRecordClosedState(selectedRecordID, false);
        }
    }

    private void changeRecordClosedState(double selectedRecordID, boolean setRecordAsClosed) {
        if (setRecordAsClosed) {
            double actualTime = System.currentTimeMillis();
            String time = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(actualTime);
            Records.updateRecordWithNewData(selectedRecordID, 9, time);
            Records.updateRecordWithNewData(selectedRecordID, 10, "TRUE");
        } else {
            Records.updateRecordWithNewData(selectedRecordID, 9, "");
            Records.updateRecordWithNewData(selectedRecordID, 10, "FALSE");
        }
        recordsTable.refresh();
        sortRecordsTableByDateCreated();
    }

    private void sortRecordsTableByDateCreated() {
        recordEntryMillisColumn.setSortType(TableColumn.SortType.DESCENDING);
        recordsTable.getSortOrder().add(recordEntryMillisColumn);
        recordEntryMillisColumn.setSortable(true);
        recordsTable.sort();
    }

    private boolean userConfirmsChange(Optional<ButtonType> result) {
        return result.isPresent() && result.get() == ButtonType.OK;
    }

    private void alignRecordsTableColumns() {
        recordUserNameColumn.setStyle("-fx-alignment: center");
        recordEntryTimeColumn.setStyle("-fx-alignment: center");
        recordExitTimeColumn.setStyle("-fx-alignment: center");
        recordVisitedNameColumn.setStyle("-fx-alignment: center");
        recordVisitorNameColumn.setStyle("-fx-alignment: center");
        recordCompanyNameColumn.setStyle("-fx-alignment: center");
        recordVisitorIDColumn.setStyle("-fx-alignment: center");
        recordCarIDColumn.setStyle("-fx-alignment: center");
        recordVisitReasonColumn.setStyle("-fx-alignment: center");
        recordIDColumn.setStyle("-fx-alignment: center");
        recordClosedColumn.setStyle("-fx-alignment: center");
    }

    private void bindRecordsTableToRecordClass() {
        recordEntryMillisColumn.setCellValueFactory(new PropertyValueFactory("recordEntryTimeMillis"));
        recordUserNameColumn.setCellValueFactory(new PropertyValueFactory("recordUserName"));
        recordEntryTimeColumn.setCellValueFactory(new PropertyValueFactory("recordEntryTime"));
        recordExitTimeColumn.setCellValueFactory(new PropertyValueFactory("recordExitTime"));
        recordVisitorNameColumn.setCellValueFactory(new PropertyValueFactory("recordVisitorName"));
        recordCompanyNameColumn.setCellValueFactory(new PropertyValueFactory("recordCompanyName"));
        recordVisitorIDColumn.setCellValueFactory(new PropertyValueFactory("recordVisitorID"));
        recordCarIDColumn.setCellValueFactory(new PropertyValueFactory("recordCarID"));
        recordVisitReasonColumn.setCellValueFactory(new PropertyValueFactory("recordVisitReason"));
        recordVisitedNameColumn.setCellValueFactory(new PropertyValueFactory("recordVisitedName"));
        recordIDColumn.setCellValueFactory(new PropertyValueFactory("recordInternalID"));
        recordClosedColumn.setCellValueFactory(new PropertyValueFactory("recordClosed"));
    }

    public void shortcutPressed(KeyEvent keyEvent) {
        if (userWantsToAddNewRow(keyEvent)) {
            addNewRow();
            sortRecordsTableByDateCreated();
        } else if (userWantsToSearchRecords(keyEvent)) {
            searchTextField.requestFocus();
        } else if (userWantsToExportData(keyEvent)) {
            exportToExcel();
        }

    }

    private boolean userWantsToExportData(KeyEvent keyEvent) {
        return keyEvent.getCode() == KeyCode.E && keyEvent.isControlDown() || keyEvent.getCode() == KeyCode.E && keyEvent.isMetaDown();
    }

    private boolean userWantsToSearchRecords(KeyEvent keyEvent) {
        return keyEvent.getCode() == KeyCode.F && keyEvent.isControlDown() || keyEvent.getCode() == KeyCode.F && keyEvent.isMetaDown();
    }

    private boolean userWantsToAddNewRow(KeyEvent keyEvent) {
        return keyEvent.getCode() == KeyCode.N && keyEvent.isControlDown() || keyEvent.getCode() == KeyCode.N && keyEvent.isMetaDown();
    }

    public void shortcutReleased(KeyEvent keyEvent) {
        if (userWantsToAddNewRow(keyEvent)) {
            startEditingNewRow();
        } else if (isNewRecord && recordsTab.isSelected()) {
            if (userInputConfirmed(keyEvent)) {
                updateRecordsTableAndMoveToNextColumn();
            }
        } else if (isNewUser && settingsTab.isSelected() && userInputConfirmed(keyEvent)) {
            updateUsersTableAndMoveToNextColumn();
        }
    }

    private void updateUsersTableAndMoveToNextColumn() {
        if (newUserColumn == 0) {
            usersTable.edit(actualUserRow, userNameColumn);
            editedUserValue = "";
            newUserColumn = 1;
        } else if (newUserColumn == 1) {
            usersTable.edit(0, userPasswordColumn);
            editedUserValue = "";
            newUserColumn = 2;
        } else if (newUserColumn == 2) {
            usersTable.edit(0, userChipCodeColumn);
            editedUserValue = "";
            isNewUser = false;
            newUserColumn = 1;
        }
    }

    private void updateRecordsTableAndMoveToNextColumn() {
        if (newRecordColumn == 2) {
            recordsTable.edit(0, recordCompanyNameColumn);
            editedRecordValue = "";
            newRecordColumn = 3;
        } else if (newRecordColumn == 3) {
            recordsTable.edit(0, recordVisitorIDColumn);
            editedRecordValue = "";
            newRecordColumn = 4;
        } else if (newRecordColumn == 4) {
            recordsTable.edit(0, recordCarIDColumn);
            editedRecordValue = "";
            newRecordColumn = 5;
        } else if (newRecordColumn == 5) {
            recordsTable.edit(0, recordVisitReasonColumn);
            editedRecordValue = "";
            newRecordColumn = 6;
        } else if (newRecordColumn == 6) {
            recordsTable.edit(0, recordVisitedNameColumn);
            editedRecordValue = "";
            newRecordColumn = 7;
        } else if (newRecordColumn == 7) {
            recordsTable.edit(0, recordIDColumn);
            editedRecordValue = "";
            isNewRecord = false;
            newRecordColumn = 2;
        }
    }

    private boolean userInputConfirmed(KeyEvent keyEvent) {
        return keyEvent.getCode() == KeyCode.ENTER || keyEvent.getCode() == KeyCode.TAB;
    }

    public void clickedOutsideTable() {
        window.requestFocus();
        deleteButton.setDisable(true);
        deleteButton.setOpacity(0.0);
    }

    public void startEditingNewRow() {
        if (recordsTab.isSelected()) {
            editNewRecord();
        } else {
            editNewUser();
        }
    }

    private void editNewUser() {
        deleteButton.setDisable(true);
        deleteButton.setOpacity(0.0);
        isNewUser = true;
        usersTable.edit(0, userNameColumn);
    }

    private void editNewRecord() {
        deleteButton.setDisable(true);
        deleteButton.setOpacity(0.0);
        isNewRecord = true;
        recordsTable.edit(0, recordVisitorNameColumn);
    }

    public void addNewRow() {
        if (recordsTab.isSelected()) {
            createNewRecord();
        } else {
            createNewUser();
        }
    }

    private void createNewRecord() {
        recordsTable.requestFocus();
        newRecordColumn = 2;
        double actualPosition = Records.counter;
        String username = Controller.userName;
        Record newRecord = new Record(Users.returnUserWithName(username), "", "", actualPosition + 1.0);
        Records.addRecord(newRecord);
        listOfRecords.setAll(Records.returnRecords());
        isNewRecord = true;
    }

    private void createNewUser() {
        usersTable.requestFocus();
        newUserColumn = 0;
        User newUser = new User("", "", "");
        Users.addUser(newUser);
        listOfUsers.setAll(Users.returnUsers());
        isNewUser = true;
        usersTable.sort();
    }

    public void newSearchEntered() {
        helpField.setText("Enter 1.12.2016 - 31.12.2016 to display records in this period.");
        double milliSecondInOneDay = 8.64E7;
        if (userEnteredDateSearch()) {
            String initialString = parseString(searchTextField.getText(), 1);
            String endingString = parseString(searchTextField.getText(), 2);
            LocalDate initialDate = stringToDate(initialString);
            LocalDate endingDate = stringToDate(endingString);
            double initialMillis = (double) initialDate.toEpochDay() * milliSecondInOneDay;
            double endingMillis = (double) endingDate.toEpochDay() * milliSecondInOneDay;
            listOfRecords.clear();
            listOfRecords.addAll(Records.returnRecordsBetween(initialMillis, endingMillis));
            updateSearchGUI();
        } else {
            searchForUserInput();
        }
        if (searchIsEmpty()) {
            clearSearchButton.setDisable(true);
            clearSearchButton.setOpacity(0.0);
            helpField.setText("CTRL+N (new), CTRL+F (find), DEL (delete), CTRL+E (export)");
        }
        sortRecordsTableByDateCreated();
    }

    private boolean searchIsEmpty() {
        return searchTextField.getText().length() < 1;
    }

    private void searchForUserInput() {
        listOfRecords.clear();
        listOfRecords.addAll(Records.returnRecordsMatching(searchTextField.getText().toLowerCase()));
        updateSearchGUI();
    }

    private void updateSearchGUI() {
        clearSearchButton.setDisable(false);
        clearSearchButton.setStyle("-fx-background-radius: 0 3 3 0");
        clearSearchButton.setOpacity(100.0);
        updateInfoStatistics();
    }

    private LocalDate stringToDate(String initialString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy", Locale.ENGLISH);
        return LocalDate.parse(initialString, formatter);
    }

    private boolean userEnteredDateSearch() {
        return searchTextField.getText().matches("\\d{1,12}[\\.]\\d{1,12}[\\.]\\d\\d\\d\\d[\\ ][\\-][\\ ]\\d{1,12}[\\.]\\d{1,12}[\\.]\\d\\d\\d\\d");
    }

    private String parseString(String text, int positionInText) {
        String date = positionInText == 1 ? text.substring(0, text.indexOf(" ")) : text.substring(text.lastIndexOf(" ") + 1, text.length());
        return date;
    }

    public void clickedToSearch() {
        deleteButton.setDisable(true);
        deleteButton.setOpacity(0.0);
    }

    public void clearSearch() {
        searchTextField.setText("");
        searchForUserInput();
        clearSearchButton.setDisable(true);
        clearSearchButton.setOpacity(0.0);
        sortRecordsTableByDateCreated();
    }

    public void deleteRecordButtonClicked() {
        showDeleteConfirmation();
    }

    private void showDeleteConfirmation() {
        if (recordsTab.isSelected()) {
            deleteRecord();
        } else {
            deleteUser();
        }
    }

    private void deleteUser() {
        User selectedUser = (User) usersTable.getSelectionModel().getSelectedItem();
        if (userWantsToChangeDefaultUser(selectedUser)) {
            showDefaultUserWarning();
        } else {
            confirmDeletingUser();
        }
    }

    private void confirmDeletingUser() {
        Alert deleteConfirmation = new Alert(Alert.AlertType.CONFIRMATION);
        deleteConfirmation.setTitle("User delete");
        deleteConfirmation.setHeaderText("Do you really want to delete this user?");
        Optional result = deleteConfirmation.showAndWait();
        if (userConfirmsChange(result)) {
            deleteSelectedRow();
        } else {
            deleteConfirmation.close();
        }
    }

    private void showDefaultUserWarning() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("ALERT!!!");
        alert.setHeaderText("Not possible to delete this user.");
        alert.showAndWait();
    }

    private void deleteRecord() {
        Alert deleteConfirmation = new Alert(Alert.AlertType.CONFIRMATION);
        deleteConfirmation.setTitle("Record delete");
        deleteConfirmation.setHeaderText("Do you really want to delete this record?");
        Optional result = deleteConfirmation.showAndWait();
        if (userConfirmsChange(result)) {
            deleteSelectedRow();
        } else {
            deleteConfirmation.close();
        }
        sortRecordsTableByDateCreated();
    }

    private void deleteSelectedRow() {
        if (recordsTab.isSelected()) {
            deleteRecordsRow();
        } else {
            deleteUsersRow();
        }
    }

    private void deleteUsersRow() {
        User selectedUser = (User) usersTable.getSelectionModel().getSelectedItem();
        usersTable.getItems().remove(selectedUser);
        Users.removeUser(selectedUser);
        recordsTable.refresh();
    }

    private void deleteRecordsRow() {
        Record selectedRecord = (Record) recordsTable.getSelectionModel().getSelectedItem();
        recordsTable.getItems().remove(selectedRecord);
        Records.removeRecord(selectedRecord);
        updateInfoStatistics();
        recordsTable.refresh();
    }

    public void exportToExcel() {
        clickedOutsideTable();
        try {
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet spreadsheet = workbook.createSheet("Zapsi Visit Data");
            spreadsheet.setColumnWidth(0, 7000);
            spreadsheet.setColumnWidth(1, 5000);
            spreadsheet.setColumnWidth(2, 7000);
            spreadsheet.setColumnWidth(3, 7000);
            spreadsheet.setColumnWidth(4, 3000);
            spreadsheet.setColumnWidth(5, 3000);
            spreadsheet.setColumnWidth(6, 5000);
            spreadsheet.setColumnWidth(7, 7000);
            spreadsheet.setColumnWidth(8, 3000);
            spreadsheet.setColumnWidth(9, 5000);
            HSSFRow headerRow = spreadsheet.createRow(0);
            headerRow.setHeightInPoints(20.0f);
            HSSFCellStyle style = workbook.createCellStyle();
            HSSFFont font = workbook.createFont();
            font.setColor((short) 10);
            font.setBold(true);
            style.setFillForegroundColor((short) 22);
            style.setFillPattern(FillPatternType.forInt(1));
            style.setFont(font);
            HSSFCell cell = headerRow.createCell(0);
            cell.setCellStyle(style);
            cell.setCellValue("Recorded by");
            cell = headerRow.createCell(1);
            cell.setCellStyle(style);
            cell.setCellValue("Entry");
            cell = headerRow.createCell(2);
            cell.setCellStyle(style);
            cell.setCellValue("Name");
            cell = headerRow.createCell(3);
            cell.setCellStyle(style);
            cell.setCellValue("Company");
            cell = headerRow.createCell(4);
            cell.setCellStyle(style);
            cell.setCellValue("ID");
            cell = headerRow.createCell(5);
            cell.setCellStyle(style);
            cell.setCellValue("Car");
            cell = headerRow.createCell(6);
            cell.setCellStyle(style);
            cell.setCellValue("Reason");
            cell = headerRow.createCell(7);
            cell.setCellStyle(style);
            cell.setCellValue("Visited");
            cell = headerRow.createCell(8);
            cell.setCellStyle(style);
            cell.setCellValue("Internal ID");
            cell = headerRow.createCell(9);
            cell.setCellStyle(style);
            cell.setCellValue("Exit");
            double amountOfRowsExcelHandles = 65532.0;
            if ((double) recordsTable.getItems().size() > amountOfRowsExcelHandles) {
                Alert changeAlert = new Alert(Alert.AlertType.WARNING);
                changeAlert.setTitle("ALERT!!!");
                changeAlert.setHeaderText("Not possible to export more than 65 532 records.");
                changeAlert.showAndWait();
            } else {
                for (int i = 0; i < recordsTable.getItems().size(); ++i) {
                    Record record = (Record) recordsTable.getItems().get(i);
                    HSSFRow dataRow = spreadsheet.createRow(i + 1);
                    dataRow.setHeightInPoints(15.0f);
                    cell = dataRow.createCell(0);
                    cell.setCellValue(record.getRecordUserName());
                    cell = dataRow.createCell(1);
                    cell.setCellValue(record.getRecordEntryTime());
                    cell = dataRow.createCell(2);
                    cell.setCellValue(record.getRecordVisitorName());
                    cell = dataRow.createCell(3);
                    cell.setCellValue(record.getRecordCompanyName());
                    cell = dataRow.createCell(4);
                    cell.setCellValue(record.getRecordVisitorID());
                    cell = dataRow.createCell(5);
                    cell.setCellValue(record.getRecordCarID());
                    cell = dataRow.createCell(6);
                    cell.setCellValue(record.getRecordVisitReason());
                    cell = dataRow.createCell(7);
                    cell.setCellValue(record.getRecordVisitedName());
                    cell = dataRow.createCell(8);
                    cell.setCellValue(record.getRecordVisitorID());
                    cell = dataRow.createCell(9);
                    cell.setCellValue(record.getRecordExitTime());
                }
                String directory = "";
                FileChooser fileChooser = new FileChooser();
                FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XLS files (*.xls)", new String[]{"*.xls"});
                fileChooser.getExtensionFilters().add(extFilter);
                File file = fileChooser.showSaveDialog(new Stage());
                FileOutputStream out = new FileOutputStream(new File(directory + file));
                workbook.write(out);
                out.close();
            }
        } catch (Exception e) {
            System.out.println("ERROR: cannot export.");
        }


    }

    public void openWebsite() throws URISyntaxException, IOException {
        Desktop desktop;
        URI uri = new URI("http://www.zapsi.eu");
        if (Desktop.isDesktopSupported() && (desktop = Desktop.getDesktop()).isSupported(Desktop.Action.BROWSE)) {
            desktop.browse(uri);
        }
    }


    public void tabChangedToRecords() {
        newRecordButton.setDisable(false);
        newRecordButton.setOpacity(100.0);
        searchTextField.setDisable(false);
        searchTextField.setOpacity(100.0);
        newRecordButton.setText("New record");
        exportButton.setDisable(false);
        exportButton.setOpacity(100.0);
        if (!searchTextField.getText().isEmpty()) {
            clearSearchButton.setOpacity(100.0);
            clearSearchButton.setDisable(false);
        }
    }

    public void keyPressedInTable(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.DELETE) {
            showDeleteConfirmation();
        }
    }

    public void enableDeleteButton() {
        deleteButton.setDisable(false);
        deleteButton.setOpacity(100.0);
    }

    public void cancelEditingRecord(TableColumn.CellEditEvent cellEditEvent) {
        editedRecordValue = "";
        cellEditEvent.consume();
        isNewRecord = false;
    }

    public void commitEditingRecord(TableColumn.CellEditEvent cellEditEvent) {
        editedRecordValue = (String) cellEditEvent.getNewValue();
        updateRecordCellData();
        updateInfoStatistics();
        editedRecordValue = "";
        recordsTable.requestFocus();
    }

    private void updateRecordCellData() {
        if (isNewRecord) {
            Records.updateRecordWithNewData(selectedRecordID, actualRecordColumn, editedRecordValue);
        } else {
            Records.updateRecordWithNewData(selectedRecordID, actualRecordColumn, editedRecordValue);
            isNewRecord = false;
        }
        editedRecordValue = "";
    }

    public void startEditingRecord(TableColumn.CellEditEvent cellEditEvent) {
        editedRecordValue = "";
        selectedRecordID = ((Record) cellEditEvent.getRowValue()).getRecordID();
        actualRecordColumn = cellEditEvent.getTablePosition().getColumn();
    }

    public void tabChangedToSettings() {
        newRecordButton.setDisable(false);
        searchTextField.setDisable(true);
        searchTextField.setOpacity(0.0);
        newRecordButton.setOpacity(100.0);
        newRecordButton.setText("New user");
        exportButton.setDisable(true);
        exportButton.setOpacity(0.0);
        clearSearchButton.setDisable(true);
        clearSearchButton.setOpacity(0.0);
    }

    public void changeDataDirectory() {
        clickedOutsideTable();
        try {
            File selectedDirectory = new DirectoryChooser().showDialog(new Stage());
            Main.directory = selectedDirectory.toString();
        } catch (Exception userClicksCancel) {
            System.out.println(userClicksCancel.toString());
        }
        preferencesTextField.setText(Main.directory);
        Main.prefs.put("LAST_OUTPUT_DIR", Main.directory);
    }

    public void commitEditingUser(TableColumn.CellEditEvent cellEditEvent) {
        editedUserValue = (String) cellEditEvent.getNewValue();
        actualUserRow = cellEditEvent.getTablePosition().getRow();
        int column = cellEditEvent.getTablePosition().getColumn();
        if (column == 1) {
            checkIfUsernameExists();
            if (usernameExists) {
                newUserColumn = 0;
            } else {
                updateUserCellData();
                editedUserValue = "";
                newUserColumn = 1;
            }
        } else {
            updateUserCellData();
            editedUserValue = "";
        }
    }

    private void updateUserCellData() {
        if (isNewUser) {
            Users.updateUserData(selectedUserName, actualUserColumn, editedUserValue);
        } else {
            Users.updateUserData(selectedUserName, actualUserColumn, editedUserValue);
            isNewRecord = false;
        }
        editedRecordValue = "";
    }

    private void checkIfUsernameExists() {
        if (Users.userExist(editedUserValue)) {
            usernameExists = true;
            Alert changeAlert = new Alert(Alert.AlertType.WARNING);
            changeAlert.setTitle("ALERT!!!");
            changeAlert.setHeaderText("Username is taken. Not possible to save this user..");
            changeAlert.showAndWait();
            usersTable.refresh();
        } else {
            usernameExists = false;
        }
    }

    public void startEditingUser(TableColumn.CellEditEvent cellEditEvent) {
        editedUserValue = "";
        selectedUserName = ((User) cellEditEvent.getRowValue()).getUserName();
        actualUserColumn = cellEditEvent.getTablePosition().getColumn();
    }

    public void tabChangedToStatistics() {
        newRecordButton.setDisable(true);
        newRecordButton.setOpacity(0.0);
        searchTextField.setDisable(true);
        searchTextField.setOpacity(0.0);
        exportButton.setDisable(true);
        exportButton.setOpacity(0.0);
        clearSearchButton.setDisable(true);
        clearSearchButton.setOpacity(0.0);
        initiateCharts();
    }

    private void initiateCharts() {
        initiateCompaniesPie();
        initiateReasonsPie();
        initiateLast30DaysChart();
    }

    private void initiateLast30DaysChart() {
        last30DaysChart.getData().clear();
        ArrayList<Record> records = Records.returnRecords();
        long actualTime = System.currentTimeMillis();
        XYChart.Series dataSeries = new XYChart.Series();
        if (records.size() > 0) {
            display30DaysChart(records, actualTime, dataSeries);
        }
    }

    private void display30DaysChart(ArrayList<Record> records, long actualTime, XYChart.Series dataSeries) {
        LocalTime now = LocalTime.now(ZoneId.systemDefault());
        int maxRecordsPerDay = 0;
        double millisecondsInOneDay = 8.64E7;
        double milliFromMidnight = millisecondsInOneDay - (double) (now.toSecondOfDay() * 1000);
        for (int currentDay = (int) daysToDisplay; currentDay >= 0; --currentDay) {
            int actualRecordsPerDay = 0;
            for (Record record : records) {
                double recordEntryTime = record.getRecordEntryTimeMillis();
                if (currentDay != (int) (((double) actualTime - recordEntryTime + milliFromMidnight) / millisecondsInOneDay) || ++actualRecordsPerDay <= maxRecordsPerDay)
                    continue;
                maxRecordsPerDay = actualRecordsPerDay;
            }
            if (currentDay == 0) {
                dataSeries.getData().add(new XYChart.Data((Object) "today", (Object) actualRecordsPerDay));
                continue;
            }
            if (currentDay == 1) {
                dataSeries.getData().add(new XYChart.Data((Object) "yesterday", (Object) actualRecordsPerDay));
                continue;
            }
            dataSeries.getData().add(new XYChart.Data((Object) ("before " + currentDay + " days"), (Object) actualRecordsPerDay));
        }
        last30DaysChart.getData().add(dataSeries);
        thirtyDaysYAxis.setUpperBound((double) maxRecordsPerDay);
    }

    private void initiateReasonsPie() {
        topReasonsPie.getData().clear();
        ArrayList<Record> records = Records.returnRecords();
        HashMap<String, Integer> data = new HashMap<>();
        double actualTime = System.currentTimeMillis();
        double maxTime = actualTime - daysToDisplay * 8.64E7;
        for (Record record : records) {
            if (!recordIsInSelectedRange(maxTime, record)) continue;
            String reason = record.getRecordVisitReason();
            if (data.containsKey(reason)) {
                int occurrence = data.get(reason);
                data.replace(reason, occurrence + 1);
                continue;
            }
            data.put(reason, 1);
        }
        int actualCount = data.values().stream().mapToInt(amount -> amount).sum();
        sortChartDataDescending(data, actualCount, topReasonsPie);
        topReasonsPie.setStartAngle(90.0);
    }

    private void initiateCompaniesPie() {
        topCompaniesPie.getData().clear();
        ArrayList<Record> records = Records.returnRecords();
        HashMap<String, Integer> data = new HashMap<>();
        double actualTime = System.currentTimeMillis();
        double maxTime = actualTime - daysToDisplay * 8.64E7;
        for (Record record : records) {
            if (!recordIsInSelectedRange(maxTime, record)) continue;
            String visitorName = record.getRecordCompanyName();
            if (data.containsKey(visitorName)) {
                int occurrence = data.get(visitorName);
                data.replace(visitorName, occurrence + 1);
                continue;
            }
            data.put(visitorName, 1);
        }
        int actualCount = data.values().stream().mapToInt(amount -> amount).sum();
        sortChartDataDescending(data, actualCount, topCompaniesPie);
        topCompaniesPie.setStartAngle(90.0);
    }

    private void sortChartDataDescending(HashMap<String, Integer> data, int finalCount, PieChart chart) {
        data.entrySet().stream().sorted((k1, k2) -> -k1.getValue().compareTo(k2.getValue())).forEach(k -> {
                    float percentage = (float) ((int) ((float) k.getValue().intValue() / (float) finalCount * 1000.0f)) / 10.0f;
                    chart.getData().add(new PieChart.Data(k.getKey() + ": " + percentage + "%", (double) k.getValue().intValue()));
                }
        );
    }

    private boolean recordIsInSelectedRange(double maxTime, Record record) {
        return record.getRecordEntryTimeMillis() > maxTime;
    }

    public void updateSlidersValue() {
        if (chartSlider.getValue() == 1.0) {
            daysToDisplay = 30.0;
            sliderInfoText.setText("last 30 days.");
        } else if (chartSlider.getValue() == 2.0) {
            daysToDisplay = 60.0;
            sliderInfoText.setText("last 60 days.");
        } else if (chartSlider.getValue() == 3.0) {
            daysToDisplay = 91.0;
            sliderInfoText.setText("last 90 days.");
        } else if (chartSlider.getValue() == 4.0) {
            daysToDisplay = 182.0;
            sliderInfoText.setText("last 182 days.");
        } else if (chartSlider.getValue() == 5.0) {
            daysToDisplay = 365.0;
            sliderInfoText.setText("last 365 days.");
        } else {
            daysToDisplay = 730.0;
            sliderInfoText.setText("last 730 days.");
        }
        initiateCharts();
    }
}
