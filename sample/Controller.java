package sample;

import javafx.animation.Animation;
import javafx.animation.TranslateTransition;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public TextField userNameInput;
    public PasswordField userPasswordInput;
    public Button loginButton;
    private Users users = new Users();
    private static TranslateTransition shakeTranslation;
    static String userName;

    public void loginUser() throws IOException {
        if (users.passwordMatchesUser(userNameInput.getText(), userPasswordInput.getText())) {
            loginToMainMenu();
        } else {
            Controller.shakePasswordEntry(userPasswordInput);
        }

    }

    private void loginToMainMenu() throws IOException {
        Main.mainProgram.close();
        userName = this.userNameInput.getText();
        Stage newWindow = new Stage();
        Parent root = FXMLLoader.load(this.getClass().getResource("MainMenu.fxml"));
        Scene mainMenu = new Scene(root, 1024.0, 568.0);
        mainMenu.getStylesheets().add(this.getClass().getResource("stylesheet.css").toExternalForm());
        newWindow.setTitle("Zapsi Visit 1.2");
        newWindow.setScene(mainMenu);
        newWindow.setMinHeight(590.0);
        newWindow.setMinWidth(1024.0);
        newWindow.show();
    }

    private static void shakePasswordEntry(Node node) {
        if (shakeTranslation == null || shakeTranslation.getNode() != node) {
            shakeTranslation = new TranslateTransition(Duration.millis(50.0), node);
        }
        shakeTranslation.setByX(10.0);
        shakeTranslation.setByX(-10.0);
        shakeTranslation.setCycleCount(10);
        shakeTranslation.setAutoReverse(true);
        if (shakeTranslation.getStatus() == Animation.Status.STOPPED) {
            shakeTranslation.playFromStart();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Users.initializeUsers();
        Records.initializeRecords();
    }
}
