package sample;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.prefs.Preferences;

public class Main extends Application {

    static  Stage mainProgram;
    static Preferences prefs;
    static String directory;

    @Override
    public void start(Stage primaryStage) throws Exception{
        setProgramDirectory();
        Parent root = FXMLLoader.load(this.getClass().getResource("sample.fxml"));
        mainProgram = primaryStage;
        mainProgram.setTitle("Login");
        mainProgram.setScene(new Scene(root, 300.0, 200.0));
        mainProgram.setResizable(false);
        mainProgram.show();
    }

    @FXML
    public void stop() {
        updateUsersFile();
        updateRecordsFile();
        System.exit(0);
        Platform.exit();
    }

    private void updateRecordsFile() {
        FileOutputStream recordsDataFile = null;
        try {
            recordsDataFile = new FileOutputStream(directory + "/records.dat");
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("ERROR: cannot save records.dat");
        }
        ObjectOutputStream dataFileWriter = null;
        try {
            dataFileWriter = new ObjectOutputStream(recordsDataFile);
        }
        catch (IOException e) {
            e.printStackTrace();
            System.out.println("ERROR: cannot save records.dat");
        }
        try {
            assert (dataFileWriter != null);
            dataFileWriter.writeObject(Records.records);
        }
        catch (IOException e) {
            e.printStackTrace();
            System.out.println("ERROR: cannot save records.dat");
        }
        try {
            assert (recordsDataFile != null);
            recordsDataFile.close();
        }
        catch (IOException e) {
            e.printStackTrace();
            System.out.println("ERROR: cannot save records.dat");
        }
    }

    private void updateUsersFile() {
        Users.removeBadlyInsertedUsers();
        FileOutputStream usersDataFile = null;
        try {
            usersDataFile = new FileOutputStream(directory + "/users.dat");
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("ERROR: cannot save users.dat");
        }
        ObjectOutputStream dataFileWriter = null;
        try {
            dataFileWriter = new ObjectOutputStream(usersDataFile);
        }
        catch (IOException e) {
            e.printStackTrace();
            System.out.println("ERROR: cannot save users.dat");
        }
        try {
            assert (dataFileWriter != null);
            dataFileWriter.writeObject(Users.users);
        }
        catch (IOException e) {
            e.printStackTrace();
            System.out.println("ERROR: cannot save users.dat");
        }
        try {
            assert (usersDataFile != null);
            usersDataFile.close();
        }
        catch (IOException e) {
            e.printStackTrace();
            System.out.println("ERROR: cannot save users.dat");
        }
    }

    private void setProgramDirectory() {
        prefs = Preferences.userNodeForPackage(this.getClass());
        loadDirectoryFromPropertyFile();
        if (directoryIsNotLoad()) {
            directory = System.getProperty("user.dir");
        }
    }

    private boolean directoryIsNotLoad() {
        return directory.length() < 1;
    }

    private void loadDirectoryFromPropertyFile() {
        try {
            directory = prefs.get("LAST_OUTPUT_DIR", "");
        }
        catch (Exception cannotSetDirectory) {
            directory = System.getProperty("user.dir");
            System.out.println("ERROR: directory not downloaded from settings.");
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
